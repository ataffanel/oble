
CROSS_COMPILE=arm-none-eabi-

CC=$(CROSS_COMPILE)gcc
AS=$(CROSS_COMPILE)as
LD=$(CROSS_COMPILE)gcc
GDB=$(CROSS_COMPILE)gdb

OPENOCD           ?= openocd
OPENOCD_DIR       ?=
OPENOCD_INTERFACE ?= $(OPENOCD_DIR)interface/cmsis-dap.cfg
OPENOCD_TARGET    ?= target/nrf51.cfg

O                 ?= -Os

INCLUDES= -I include/nrf -I include/cm0 -I include/

PROCESSOR = -mcpu=cortex-m0 -mthumb
NRF= -DNRF51
PROGRAM=oble

CFLAGS=$(PROCESSOR) $(NRF) $(INCLUDES) -g3 $(O) -Wall
ASFLAGS=$(PROCESSOR)
LDFLAGS=$(PROCESSOR) $(O) -g3 --specs=nano.specs -Wl,-Map=$(PROGRAM).map

LDFLAGS += -T gcc_nrf51_mbs_xxaa.ld

# OBLE Stack
OBJS += src/oble/oble_link.o src/oble/oble_llc.o
OBJS += src/oble/oble_l2cap.o src/oble/oble_att.o src/oble/uuid.o
OBJS += src/oble/oble_adv.o src/oble/gattTable.o src/oble/gap.o

# Example
OBJS += src/main.o src/uart.o gcc_startup_nrf51.o system_nrf51.o

all: $(PROGRAM).elf $(PROGRAM).hex $(PROGRAM).bin
	arm-none-eabi-size $(PROGRAM).elf

$(PROGRAM).hex: $(PROGRAM).elf
	arm-none-eabi-objcopy $^ -O ihex $@

$(PROGRAM).bin: $(PROGRAM).elf
	arm-none-eabi-objcopy $^ -O binary $@

$(PROGRAM).elf: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^

clean:
	rm -f $(PROGRAM).bin $(PROGRAM).elf $(PROGRAM).hex $(PROGRAM).map $(OBJS)


flash: $(PROGRAM).hex
	$(OPENOCD) -d2 -f $(OPENOCD_INTERFACE) -f $(OPENOCD_TARGET) -c init -c targets -c "reset halt" \
                 -c "flash write_image erase $(PROGRAM).hex" -c "verify_image $(PROGRAM).hex" -c "reset halt" \
	               -c "reset run" -c shutdown

flash_jlink: $(PROGRAM).bin
	JLinkExe -if swd -device nrf51822 -speed 4000 flash.jlink

reset_jlink:
	JLinkExe -if swd -device nrf51822 -speed 4000 reset.jlink

gdbserver_jlink:
	JLinkGDBServer -if swd -device nrf51822 -speed 4000

reset:
	$(OPENOCD) -d2 -f $(OPENOCD_INTERFACE) -f $(OPENOCD_TARGET) -c init -c targets -c "mww 0x40000544 0x01" -c reset -c shutdown

openocd: $(PROGRAM).elf
	$(OPENOCD) -d2 -f $(OPENOCD_INTERFACE) -f $(OPENOCD_TARGET) -c init -c targets

gdb: $(PROGRAM).elf
	$(GDB) -ex "target remote localhost:3333" -ex "monitor reset halt" $^

gdb_jlink: $(PROGRAM).elf
	$(GDB) -ex "target remote localhost:2331" -ex "monitor reset" $^
