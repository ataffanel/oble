/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Copyright (c) 2014 Bitcraze AB, all right reserved
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <nrf.h>
#include <nrf_gpio.h>

#include <uart.h>

#include <oble.h>
#include <oble/adv.h>
#include <oble/gap.h>
#include <oble/gatt.h>

char bitcrazeBaseUuid[16] = {0x08, 0x9A, 0x0A, 0xC0, 0xb7, 0x43, 0x7b, 0x94,
	                           0x9e, 0x4f, 0x7f, 0x1c, 0x00, 0x00, 0x00, 0x00};
#define CRAZYFLIE_SERVICE_UUID 0x0201
#define CRTP_CHARACTERISTIC_UUID 0x0202

// Advertisement configuration
static obleAdv_t adv;

// GATT configuration
obleGattService_t crazyflieService;
obleGattChar_t crtpChar;
// obleGattChar_t crtpupChar;
// obleGattChar_t crtpdownChar;

#define LOCAL_NAME "Test"

char crtpBuffer[32] = {0xff, 0};

int main()
{
	// Start crystal clocks
	NRF_CLOCK->EVENTS_HFCLKSTARTED = 0UL;
	NRF_CLOCK->TASKS_HFCLKSTART = 1UL;
	while(!NRF_CLOCK->EVENTS_HFCLKSTARTED);
	NRF_CLOCK->EVENTS_HFCLKSTARTED = 0UL;

	NRF_CLOCK->LFCLKSRC = (CLOCK_LFCLKSRC_SRC_Synth << CLOCK_LFCLKSRC_SRC_Pos);

	NRF_CLOCK->EVENTS_LFCLKSTARTED = 0UL;
	NRF_CLOCK->TASKS_LFCLKSTART = 1UL;
	while(!NRF_CLOCK->EVENTS_LFCLKSTARTED);
	NRF_CLOCK->EVENTS_LFCLKSTARTED = 0UL;

	// Initialize subsystems
	uartInit();

	// Setup advertisement
	obleAdvAddressType_t addressType = (NRF_FICR->DEVICEADDRTYPE == FICR_DEVICEADDRTYPE_DEVICEADDRTYPE_Public)?publicAddress:randomAddress;
	obleAdvInitAdv(&adv, (void*)NRF_FICR->DEVICEADDR, addressType, 200);
	// Adv packet
	obleGapAppendFlag(&adv, advPacket, 0x06);
	obleGapAppendLocalName(&adv, advPacket, LOCAL_NAME);
	obleGapAppendManufacturerData(&adv, advPacket, 0x01c5, "", 0);  // Bitcraze data
	// Scan packet
	obleAdvAppendItemToPacket(&adv, scanPacket, GAP_SERVICE_DATA_16BITS_UUID, "\x0f\x18\x42", 3);  // Battery data`
	obleAdvAppendItemToPacket(&adv, scanPacket, GAP_SERVICE_DATA_16BITS_UUID, "\x09\x18\x24\0\0\0", 6);  // Temperature data
	// Enable the new advertisement configuration
	obleAdvSetActiveAdvertisement(&adv);

	// Initialize GATT configuration
	int bitcrazeUuidBase = uuidRegisterBase(bitcrazeBaseUuid);
	obleGattInit(LOCAL_NAME);
	obleGattServiceInit(&crazyflieService,  uuidFromUuid16AndBase(bitcrazeUuidBase, CRAZYFLIE_SERVICE_UUID));
	obleGattAddService(&crazyflieService);
	obleGattCharInit(&crtpChar, uuidFromUuid16AndBase(bitcrazeUuidBase, CRTP_CHARACTERISTIC_UUID));
	obleGattCharSetValue(&crtpChar, crtpBuffer, 1);
	obleGattServiceAddChar(&crazyflieService, &crtpChar);
	obleGattBuildTable();

	uartPuts("\n\r\n\rStarting advertising\r\n");

	// Main loop
	while(1) {
		if (obleGetState() == state_standby) {
			obleLinkInit();
			obleLinkStartAdvertising();
		}
	}
}
