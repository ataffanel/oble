// Implements a GATT table defined at startup by the user program. Interfaces
// the ATT server using the interface defined in att_table.h

#include <oble.h>
#include <oble/gatt.h>
#include <att_table.h>
#include <att_pdu.h>

#include <string.h>

// Top level list of services
static obleGattService_t *services;

// Automatically added service
static obleGattService_t genericAccessService;
static obleGattChar_t deviceNameChar;

// Attribute access functions
static int readService(void* context, char* buffer ,int* length, int maxLength);
static int readCharAttribute(void* context, char* buffer ,int* length, int maxLength);
static int readCharValue(void* context, char* buffer ,int* length, int maxLength);
// static int readCharDescriptor(void* context, char* buffer ,int* length, int maxLength);
static int writeNotPermited(void *context, char* buffer, int length);

static int getLengthService(void* context);
static int getLengthCharAttribute(void* context);
static int getLengthCharValue(void* context);

// List of attributes (set when the gatt table is generated)
static struct attElement_s *attributes;

// Temp variables
static char tempBuffer[27];


//////////////////
// High level functions to build the GATT table

void obleGattInit(char *deviceName)
{
  services = NULL;
  attributes = NULL;

  obleGattServiceInit(&genericAccessService, uuidFromUuid16(0x1800));
  obleGattAddService(&genericAccessService);

  obleGattCharInit(&deviceNameChar, uuidFromUuid16(0x2A00));
  obleGattCharSetValue(&deviceNameChar, deviceName, strlen(deviceName));
  obleGattServiceAddChar(&genericAccessService, &deviceNameChar);
}

void obleGattAddService(obleGattService_t *service)
{
  obleGattService_t *ptr = services;

  service->next = NULL;

  if (ptr == NULL) {
    services = service;
  } else {
    while (ptr->next != NULL) {
      ptr = ptr->next;
    }

    ptr->next = service;
  }
}

static void appendToAttributes(struct attElement_s *att)
{
  att->next = NULL;
  if (attributes == NULL) {
    attributes = att;
  } else {
    struct attElement_s *ptr = attributes;
    while (ptr->next != NULL) {
      ptr = ptr->next;
    }
    ptr->next = att;
  }
}

void obleGattBuildTable()
{
  uint16_t currentHandle = 1;
  obleGattService_t *currentService = services;
  obleGattChar_t * currentChar = NULL;

  attributes = NULL;

  while (currentService != NULL) {
    currentService->attribute.handle = currentHandle++;
    appendToAttributes(&currentService->attribute);

    // ToDo: Push includes

    currentChar = currentService->characteristics;

    while(currentChar != NULL) {
      currentChar->attribute.handle = currentHandle++;
      appendToAttributes(&currentChar->attribute);
      currentChar->value.handle = currentHandle++;
      appendToAttributes(&currentChar->value);

      currentChar = currentChar->next;
    }


    currentService = currentService->next;
  }
}

void obleGattServiceInit(obleGattService_t *service, uuid_t uuid)
{
  memset(service, 0, sizeof(*service));
  service->uuid = uuid;
  service->primary = true;

  service->attribute.uuid = uuidFromUuid16(OBLE_GATT_PRIMARY_SERVICE);
  service->attribute.context = service;
  service->attribute.read = readService;
  service->attribute.write = writeNotPermited;
  service->attribute.getLength = getLengthService;
}

void obleGattServiceAddChar(obleGattService_t *service, obleGattChar_t *characteristic)
{
  obleGattChar_t *ptr = service->characteristics;

  characteristic->next = NULL;

  while (ptr != NULL && ptr->next != NULL) {
    ptr = ptr->next;
  }

  if (ptr == NULL) {
    service->characteristics = characteristic;
  } else {
    ptr->next = characteristic;
  }
}

void obleGattCharInit(obleGattChar_t *characteristic, uuid_t uuid)
{
  memset(characteristic, 0, sizeof(*characteristic));
  characteristic->uuid = uuid;
  characteristic->accessRight = 0x02;

  characteristic->attribute.uuid = OBLE_GATT_CHARACTERISTIC;
  characteristic->attribute.context = characteristic;
  characteristic->attribute.read = readCharAttribute;
  characteristic->attribute.write = writeNotPermited;
  characteristic->attribute.getLength = getLengthCharAttribute;

  characteristic->value.uuid = uuid;
  characteristic->value.context = characteristic;
  characteristic->value.read = readCharValue;
  characteristic->value.write = writeNotPermited;
  characteristic->value.getLength = getLengthCharValue;
}

void obleGattCharSetValue(obleGattChar_t *characteristic, char *value, int length)
{
  memcpy(characteristic->valueBuffer, value, (length > ATT_MTU)?ATT_MTU:length);
  characteristic->valueLength = length;
}

//////////////
// Att table value access function
static int readService(void *context, char* buffer, int* length, int maxLength)
{
  int bufferLength;
  obleGattService_t *service = context;

  if(uuidIsUuid16(service->uuid)) {
    bufferLength = 2;
    uint16_t uuid = uuidFromUuid16(service->uuid);
    memcpy(tempBuffer, &uuid, sizeof(uint16_t));
  } else {
    bufferLength = 16;
    uuidGetUuid128(tempBuffer, service->uuid);
  }

  if (bufferLength > maxLength) {
    *length = maxLength;
  } else {
    *length = bufferLength;
  }

  memcpy(buffer, tempBuffer, *length);

  return 0;
}

static int readCharAttribute(void* context, char* buffer ,int* length, int maxLength)
{
  int tempLength = 0;
  obleGattChar_t * characteristic = context;

  tempBuffer[0] = characteristic->accessRight;
  memcpy(&tempBuffer[1], &characteristic->value.handle, 2);
  if (uuidIsUuid16(characteristic->uuid)) {
    tempLength = 5;
    uint16_t uuid = uuidGetUuid16(characteristic->uuid);
    memcpy(&tempBuffer[3], &uuid, 2);
  } else {
    tempLength = 19;
    uuidGetUuid128(&tempBuffer[3], characteristic->uuid);
  }
  *length = (tempLength>maxLength)?maxLength:tempLength;
  memcpy(buffer, tempBuffer, *length);

  return 0;
}

static int readCharValue(void* context, char* buffer ,int* length, int maxLength)
{
  obleGattChar_t * characteristic = context;

  *length = (characteristic->valueLength > maxLength)?maxLength:characteristic->valueLength;
  memcpy(buffer, characteristic->valueBuffer, *length);
  return 0;
}

static int writeNotPermited(void *context, char* buffer, int length)
{
  return ATT_ERROR_WRITE_NOT_PERMITED;
}

static int getLengthService(void* context)
{
  obleGattService_t * service = context;
  int length = 16;

  if (uuidIsUuid16(service->uuid)) {
    length = 2;
  }

  return length;
}

static int getLengthCharAttribute(void* context)
{
  obleGattChar_t * characteristic = context;
  int length = 19;

  if (uuidIsUuid16(characteristic->uuid)) {
    length = 5;
  }

  return length;
}

static int getLengthCharValue(void* context)
{
  obleGattChar_t * characteristic = context;
  return characteristic->valueLength;
}

/////////////
// Att table search function
struct attElement_s * obleAttTableGetNextElement(int start, int end) {
  struct attElement_s *ptr = attributes;

  while (ptr != NULL && ptr->handle < start) {
    ptr = ptr->next;
  }

  if (ptr == NULL || ptr->handle > end) {
    return NULL;
  }

  return ptr;
}

struct attElement_s * obleAttTableGetElementByType(int start, int end, uuid_t uuid) {
  struct attElement_s *ptr = attributes;

  while (ptr != NULL && (ptr->handle < start || !uuidIsEqual(ptr->uuid, uuid))) {
    ptr = ptr->next;
  }

  if (ptr == NULL || ptr->handle > end) {
    return NULL;
  }

  return ptr;
}

static char comparisonBuffer[27];

struct attElement_s * obleAttTableGetElementByTypeValue(int start, int end, uuid_t uuid,
                                                        size_t length, char* value) {
  struct attElement_s *ptr = attributes;
  int elementLength;

  while (ptr != NULL) {

    if (ptr->handle > end) {
      break;
    }

    if (ptr->handle >= start) {
      ptr->read(ptr->context, comparisonBuffer, &elementLength, 27);
      if (uuidIsEqual(ptr->uuid, uuid) &&
          (elementLength == length) &&
          !memcmp(comparisonBuffer, value, length)) {
        break;
      }
    }

    ptr = ptr->next;
  }

  if (ptr == NULL || ptr->handle > end) {
    return NULL;
  }

  return ptr;
}

int obleAttTableGetGroupByType(int start, int end, uuid_t uuid, uint16_t *handle, uint16_t *endGroup)
{
  if (!uuidIsEqual(uuidFromUuid16(OBLE_GATT_PRIMARY_SERVICE), uuid)) {
    return ATT_ERROR_UNSUPPORTED_GROUP_TYPE;
  }

  struct attElement_s *startElement = obleAttTableGetElementByType(start, end, uuid);

  if (startElement == NULL) {
    return ATT_ERROR_ATTRIBUTE_NOT_FOUND;
  }

  *handle = startElement->handle;

  struct attElement_s *endElement = obleAttTableGetElementByType(startElement->handle+1, 0xffff, uuid);

  if (endElement == NULL) {
    *endGroup = 0xffff;
  } else {
    *endGroup = endElement->handle-1;
  }

  return 0;
}
