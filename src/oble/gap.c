#include <oble/gap.h>

#include <oble/adv.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

bool obleGapAppendFlag(obleAdv_t *adv, obleAdvPacketType_t packetType, uint8_t flags)
{
  return obleAdvAppendItemToPacket(adv, packetType, GAP_FLAGS, (void*)&flags, 1);
}

bool obleGapAppendLocalName(obleAdv_t *adv, obleAdvPacketType_t packetType, char* name)
{
  return obleAdvAppendItemToPacket(adv, packetType, GAP_LOCAL_NAME, name, strlen(name));
}

bool obleGapAppendManufacturerData(obleAdv_t *adv, obleAdvPacketType_t packetType, uint16_t manufacturerId, char* data, int length)
{
  bool result = true;
  uint8_t type = GAP_MANUFACTURER_SPECIFIC_DATA;
  uint8_t totalLength = length + 3;

  if (obleAdvGetPacketFreeSpace(adv, packetType) < totalLength+1) {
    return false;
  }

  result &= obleAdvAppendRawToPacket(adv, packetType, &totalLength, 1);
  result &= obleAdvAppendRawToPacket(adv, packetType, &type, 1);
  result &= obleAdvAppendRawToPacket(adv, packetType, &manufacturerId, 2);
  result &= obleAdvAppendRawToPacket(adv, packetType, data, length);

  return result;
}
