/* Link Layer Control implementation */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <oble.h>
#include <uart.h>

#include "ll_priv.h"
//#include <oble_ll.h>

#define VERSION_REQ 0x01
#define VERSION_RSP 0x02
#define ERROR

#define LL_CONNECTION_UPDATE_REQ  0x00
#define LL_CHANNEL_MAP_REQ        0x01
#define LL_TERMINATE_IND          0x02
#define LL_ENC_REQ                0x03
#define LL_ENC_RSP                0x04
#define LL_START_ENC_REQ          0x05
#define LL_START_ENC_RSP          0x06
#define LL_UNKNOWN_RSP            0x07
#define LL_FEATURE_REQ            0x08
#define LL_FEATURE_RSP            0x09
#define LL_PAUSE_ENC_REQ          0x0A
#define LL_PAUSE_ENC_RSP          0x0B
#define LL_VERSION_IND            0x0C

struct llcPdu_s {
  uint8_t opcode;
  union {
    struct {
      uint8_t versNr;
      uint16_t compId;
      uint16_t subversNr;
    } __attribute__((packed)) version_ind;
    struct {
      uint8_t winSize;
      uint16_t winOffset;
      uint16_t interval;
      uint16_t latency;
      uint16_t timeout;
      uint16_t instant;
    } __attribute__((packed)) connection_update_req;
  };
} __attribute__((packed));

static bool versionSent;

void obleLlcNewConnection()
{
  versionSent = false;
}

// Returns true if the packet should be sent back. False otherwise
bool obleLlcPacketRx(struct dataPacket_s *packet) {
  struct llcPdu_s *pdu = (struct llcPdu_s*)packet->payload;
  bool sendResponse = false;
  char buffer[4];

  switch (pdu->opcode) {
    case LL_FEATURE_REQ:
      uartPuts("\r\nLLC Feature request");
      packet->payload[0] = LL_FEATURE_RSP;
      packet->payload[1] = 0;
      packet->payload[2] = 0;
      packet->payload[3] = 0;
      packet->payload[4] = 0;
      packet->payload[5] = 0;
      packet->payload[6] = 0;
      packet->payload[7] = 0;
      sendResponse = true;
      break;
    case LL_VERSION_IND:
      uartPuts("\r\nLLC Version request");
      if (versionSent == false) {
        versionSent = true;
        pdu->version_ind.versNr = 0x08;   //Bluetooth 4.2
        pdu->version_ind.compId = 0x01C5; // Bitcraze AB
        pdu->version_ind.subversNr = 1;
        sendResponse = true;
      } else
        sendResponse = false;
      break;
    case LL_CONNECTION_UPDATE_REQ:
      setupWindow(pdu->connection_update_req.winSize,
                  pdu->connection_update_req.winOffset,
                  pdu->connection_update_req.interval,
                  pdu->connection_update_req.latency,
                  pdu->connection_update_req.timeout,
                  pdu->connection_update_req.instant);
      break;
    case LL_TERMINATE_IND:
      obleLinkStopConnection();
      break;
    default:
      uartPuts("\r\nUknown LLC packet ");
      sprintf(buffer, "%x", pdu->opcode);
      uartPuts(buffer);
      //Not implemented control opcode
      packet->header.length = 2;
      packet->payload[1] = packet->payload[0];
      packet->payload[0] = LL_UNKNOWN_RSP;
      sendResponse = true;
      break;
  }

  return sendResponse;
}
