#include <oble/uuid.h>
#include <string.h>

// Bluetooth base UUID is 00000000-0000-1000-8000-00805F9B34FB
static char bluetoothBase[16] = {0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00,
                             0x00, 0x80,
                             0x00, 0x10,
                             0x00, 0x00,
                             0x00, 0x00, 0x00, 0x00};

#define UUID_POS_LOW 12
#define UUID_POS_HIGH 13

char uuidBases[UUID_BASETABLE_LENGTH][16]  __attribute__ ((weak));
int basesTableTop = 0;

uuid_t uuidFromUuid16AndBase(int base, int uuid16) {
  if (base <= UUID_BASETABLE_LENGTH) {
    return (((uint32_t)base) << 16) | (uuid16 & 0x0ffff);
  }

  return UUID_INVALID;
}

static bool isSameBase(char* uuid1, char* uuid2) {
  bool equal = true;
  equal &= (memcmp(uuid1, uuid2, UUID_POS_LOW) == 0);
  equal &= (memcmp(uuid1+UUID_POS_HIGH+1, uuid2+UUID_POS_HIGH+1, 2) == 0);
  return equal;
}

uuid_t uuidFromUuid128(char * uuid128) {
  uuid_t uuid;

  // Find and set base
  if (isSameBase(uuid128, bluetoothBase)) {
    // Bluetooth UUID base is 0
    uuid = 0;
  } else {
    uint32_t index = 1;
    for (index = 1; index <= UUID_BASETABLE_LENGTH; index++) {
      if (isSameBase(uuid128, uuidBases[index-1])) {
        break;
      }
    }

    if (index > UUID_BASETABLE_LENGTH) {
      return UUID_INVALID;
    }

    uuid = index << 16;
  }

  uuid |= (((uint16_t)uuid128[UUID_POS_HIGH]) << 8) | uuid128[UUID_POS_LOW];

  return uuid;
}

int uuidRegisterBase(char * baseUuid)
{
  // Check if the table is full
  if (basesTableTop+1 >= UUID_BASETABLE_LENGTH) {
    return -1;
  }

  // Insert new base in table
  memcpy(&uuidBases[basesTableTop], baseUuid, 16);
  basesTableTop++;

  // We return the index +1 since 0 is the bluetooth UUID and is stored
  // separatly in flash.
  return basesTableTop;
}

bool uuidGetUuid128(char *out, uuid_t uuid) {
  int baseIndex = uuidGetBase(uuid);

  if (baseIndex == 0) {
    memcpy(out, bluetoothBase, 16);
  } else if (baseIndex <= UUID_BASETABLE_LENGTH) {
    memcpy(out, uuidBases[baseIndex-1], 16);
  } else {
    return false;
  }

  int uuid16 = uuid & 0x0000ffff;

  out[UUID_POS_LOW] = uuid16 & 0x00ff;
  out[UUID_POS_HIGH] = (uuid16 >> 8) & 0x00ff;

  return true;
}

bool uuidIsEqual(uuid_t uuid1, uuid_t uuid2) {
  return (uuid1 & 0xFFFF0000) != UUID_INVALID &&
         (uuid2 & 0xFFFF0000) != UUID_INVALID &&
         uuid1 == uuid2;
}
