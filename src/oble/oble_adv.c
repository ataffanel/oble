#include "oble/adv.h"
#include <string.h>

void obleAdvInitAdv(obleAdv_t *adv, const char address[6], obleAdvAddressType_t addressType, uint32_t period_ms)
{
  memset(adv, 0, sizeof(obleAdv_t));
  adv->period_ms = period_ms;
  memcpy(adv->address, address, 6);

  adv->advPacket.header.type = adv_ind;
  adv->advPacket.header.txAddr = addressType;
  adv->advPacket.header.length = 6;
  memcpy(adv->advPacket.payload, address, 6);

  adv->scanPacket.header.type = scan_rsp;
  adv->scanPacket.header.txAddr = addressType;
  adv->scanPacket.header.length = 6;
  memcpy(adv->scanPacket.payload, address, 6);
}

static bool appendItemToPacket(struct advPacket_s * pk, uint8_t type, char* value, int length)
{
  if ((pk->header.length + length + 2) > OBLE_ADV_PAYLOAD_LENGTH) {
    return false;
  }

  pk->payload[pk->header.length++] = length+1;
  pk->payload[pk->header.length++] = type;
  memcpy(&pk->payload[pk->header.length], value, length);
  pk->header.length += length;

  return true;
}

static bool appendRawToPacket(struct advPacket_s * pk, char * value, int length)
{
  if ((pk->header.length + length) > OBLE_ADV_PAYLOAD_LENGTH) {
    return false;
  }

  memcpy(&pk->payload[pk->header.length], value, length);
  pk->header.length += length;

  return true;
}

bool obleAdvAppendItemToPacket(obleAdv_t *adv, obleAdvPacketType_t packetType, uint8_t type, char *value, int length)
{
  bool result = false;

  if (packetType == advPacket) {
    result = appendItemToPacket(&adv->advPacket, type, value, length);
  } else if (packetType == scanPacket) {
    result = appendItemToPacket(&adv->scanPacket, type, value, length);
  }

  return result;
}

bool obleAdvAppendRawToPacket(obleAdv_t *adv, obleAdvPacketType_t packetType, void *value, int length)
{
  bool result = false;

  if (packetType == advPacket) {
    result = appendRawToPacket(&adv->advPacket, value, length);
  } else if (packetType == scanPacket) {
    result = appendRawToPacket(&adv->scanPacket, value, length);
  }

  return result;
}

void obleAdvSetActiveAdvertisement(obleAdv_t *adv)
{
  obleLinkSetAdvPackets(&adv->advPacket, &adv->scanPacket);
	obleLinkAdvSetAdvPeriod(adv->period_ms);
}

int obleAdvGetPacketFreeSpace(obleAdv_t *adv, obleAdvPacketType_t packetType)
{
  int result = 0;

  if(packetType == advPacket) {
    result = OBLE_ADV_PAYLOAD_LENGTH - adv->advPacket.header.length;
  } else if (packetType == scanPacket) {
    result = OBLE_ADV_PAYLOAD_LENGTH - adv->scanPacket.header.length;
  }

  return result;
}
