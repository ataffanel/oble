#include <stdint.h>
#include <stdbool.h>

#include <oble.h>
#include "ll_priv.h"

#define L2CAP_ATT 0x0004

struct l2capPdu_s {
  uint16_t length;
  uint16_t type;
} __attribute__((packed));

bool obleL2capPacketRx(struct dataPacket_s *packet)
{
  struct l2capPdu_s *pdu = (void*)packet->payload;

  switch (pdu->type) {
    case L2CAP_ATT:
      return obleAttPacketRx(packet);
      break;
    default:
      return false;
      break;
  }

  return false;
}
