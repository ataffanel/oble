/* BLE Link Layer implementation for nRF51822
 */
#include <nrf.h>

#include <nrf_gpio.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <uart.h>

#include <oble.h>

#include "ll_priv.h"

// #define uartPuts(...)

#define US2RTC(A) ((A)*32768ull)/1000000ull;

uint32_t advInterval = US2RTC(100e3);

static void obleInit();

// Linklayer handles one state at a time
obleLlState_t llState = state_standby;

enum {adv_idle, adv_tx, adv_rx, adv_txscan} advState = adv_idle;
static int advChannel = 37;

enum {data_rx, data_tx} dataState = data_rx;

typedef enum {sca_500ppm=0, sca_250ppm=1, sca_150ppm=2, sca_100ppm=3, sca_75ppm=4, sca_50ppm=5, sca_30ppm=6, sca_20ppm=7} sca_t;
const int sca_ppm[] = {500, 250, 150, 100, 75, 50, 30, 20};

//static enum {connected_idle, connected_connecting, connected_connected} connectedState = connected_idle;


// Frequency BLE channels
//                                0   1   2   3   4   5   6   7   8   9
const uint8_t channelFreq[40] = { 4,  6,  8, 10, 12, 14, 16, 18, 20, 22,  //  0
                                 24, 28, 30, 32, 34, 36, 38, 40, 42, 44,  // 10
                                 46, 48, 50, 52, 54, 56, 58, 60, 62, 64,  // 20
                                 66, 68, 70, 72, 74, 76, 78,  2, 26, 80   // 30
                                };

struct advPacket_s *advPacket;

struct advPacket_s advRxPacket;
struct advPacket_s *advTxPacket;

struct dataPacket_s * dataTxQueue;
struct dataPacket_s * dataRxQueue;
struct dataPacket_s * dataParkingQueue;

struct dataPacket_s dataDummyPacket;
struct dataPacket_s dataDummyTxPacket;

static inline void queue_put(struct dataPacket_s **head, struct dataPacket_s* pk) {
  struct dataPacket_s *prev;

  pk->next = NULL;

  if (*head == NULL) {
    *head = pk;
  } else {
    prev = *head;

    while (prev->next) {
      prev = prev->next;
    }

    prev->next = pk;
  }
}

static inline struct dataPacket_s * queue_get(struct dataPacket_s **head) {
  struct dataPacket_s *pk = *head;

  if (pk) {
    *head = pk->next;
  }

  return pk;
}

static struct {
  // Connection status
  uint16_t connEventCounter;
  int instant;  // Instant at which window shall be applied (-1 if disabled)

  uint8_t nesn:1;
  uint8_t sn:1;

  // Connected state packets
  struct dataPacket_s *dataTx;      // To be sent packet
  struct dataPacket_s *dataLastTx;  // Last sent packet
  struct dataPacket_s *dataRx;      // Received packet

  // Connection parameters
  uint32_t accessAddress;
  uint32_t crcInit;

  // All times are in us
  int windowSize;
  int winOffset;

  // Current configuration until a window
  uint32_t interval;
  uint32_t latency;
  uint32_t timeout;

  // New configuration to apply at a window
  uint32_t newInterval;
  uint32_t newLatency;
  uint32_t newTimeout;

  uint8_t chM[5];
  uint8_t chTable[40];
  uint8_t usedChannel;

  int hop;
  sca_t masterSca;
  sca_t slaveSca;

  int unmappedChannel;

  uint32_t timeSinceAnchor;
} connectedContext;

static void setChM(const uint8_t *chM) {
  int i, j, ch, k=0;

  connectedContext.usedChannel = 0;
  memcpy(connectedContext.chM, chM, 5);

  for (i=0; i<40; i++) {
    j=0; ch=i;
    while (ch>7) {
      ch -= 8;
      j++;
    }
    if ((connectedContext.chM[j] & (1<<ch)) != 0) {
      connectedContext.chTable[k++] = i;
    }
    connectedContext.usedChannel = k;
  }
}

static void setChannel(int channel)
{
	int i=0, ch = channel;
  //if(!(channel<40)) while(1);

  while (ch>7) {
    ch -= 8;
    i++;
  }

  if (!(connectedContext.chM[i] & (1<<ch))) {
    uartPuts("Disabled channel!");
    channel = connectedContext.chTable[channel % connectedContext.usedChannel];
  }

	NRF_RADIO->FREQUENCY 	 = channelFreq[channel];
	NRF_RADIO->DATAWHITEIV = channel;
}

static void radioInit(bool advertising)
{
  // Radio configuration
  NRF_RADIO->TXPOWER = (RADIO_TXPOWER_TXPOWER_0dBm << RADIO_TXPOWER_TXPOWER_Pos);
  NRF_RADIO->MODE 	 = (RADIO_MODE_MODE_Ble_1Mbit << RADIO_MODE_MODE_Pos);

  if (advertising) {
    // Header format for advertising PDU
    NRF_RADIO->PCNF0 =  (
                            (((1UL) << RADIO_PCNF0_S0LEN_Pos) & RADIO_PCNF0_S0LEN_Msk)    // length of S0 field in bytes 0-1.
                          | (((2UL) << RADIO_PCNF0_S1LEN_Pos) & RADIO_PCNF0_S1LEN_Msk)    // length of S1 field in bits 0-8.
                          | (((6UL) << RADIO_PCNF0_LFLEN_Pos) & RADIO_PCNF0_LFLEN_Msk)    // length of length field in bits 0-8.
                        );

    // Packet format
    NRF_RADIO->PCNF1 =  (
                            (((37UL) << RADIO_PCNF1_MAXLEN_Pos)  & RADIO_PCNF1_MAXLEN_Msk)   // maximum length of payload in bytes [0-255]
                          | (((0UL)  << RADIO_PCNF1_STATLEN_Pos) & RADIO_PCNF1_STATLEN_Msk) // expand the payload with N bytes in addition to LENGTH [0-255]
                          | (((3UL)  << RADIO_PCNF1_BALEN_Pos)   & RADIO_PCNF1_BALEN_Msk)    // base address length in number of bytes.
                          | (((RADIO_PCNF1_ENDIAN_Little) << RADIO_PCNF1_ENDIAN_Pos)  & RADIO_PCNF1_ENDIAN_Msk)   // endianess of the S0, LENGTH, S1 and PAYLOAD fields.
                          | (((1UL)  << RADIO_PCNF1_WHITEEN_Pos) & RADIO_PCNF1_WHITEEN_Msk) // enable packet whitening
                        );

    NRF_RADIO->CRCINIT = 0x555555;    // Initial value of CRC

    // Standard advertising address : 0x8E89BED6
    NRF_RADIO->PREFIX0   = 0x8e;
    NRF_RADIO->BASE0     = 0x89bed600;
  } else {
    // Header format for data PDU
    NRF_RADIO->PCNF0 =  (
                            (((1UL) << RADIO_PCNF0_S0LEN_Pos) & RADIO_PCNF0_S0LEN_Msk)    // length of S0 field in bytes 0-1.
                          | (((0UL) << RADIO_PCNF0_S1LEN_Pos) & RADIO_PCNF0_S1LEN_Msk)    // length of S1 field in bits 0-8.
                          | (((8UL) << RADIO_PCNF0_LFLEN_Pos) & RADIO_PCNF0_LFLEN_Msk)    // length of length field in bits 0-8.
                        );

    // Packet format
    NRF_RADIO->PCNF1 =  (
                            (((27UL) << RADIO_PCNF1_MAXLEN_Pos)  & RADIO_PCNF1_MAXLEN_Msk)   // maximum length of payload in bytes [0-255]
                          | (((0UL)  << RADIO_PCNF1_STATLEN_Pos) & RADIO_PCNF1_STATLEN_Msk) // expand the payload with N bytes in addition to LENGTH [0-255]
                          | (((3UL)  << RADIO_PCNF1_BALEN_Pos)   & RADIO_PCNF1_BALEN_Msk)    // base address length in number of bytes.
                          | (((RADIO_PCNF1_ENDIAN_Little) << RADIO_PCNF1_ENDIAN_Pos)  & RADIO_PCNF1_ENDIAN_Msk)   // endianess of the S0, LENGTH, S1 and PAYLOAD fields.
                          | (((1UL)  << RADIO_PCNF1_WHITEEN_Pos) & RADIO_PCNF1_WHITEEN_Msk) // enable packet whitening
                        );

    NRF_RADIO->CRCINIT = connectedContext.crcInit;    // Initial value of CRC

    // Access address as communicated by connection request
    NRF_RADIO->PREFIX0   = (connectedContext.accessAddress >> 24) & 0x000000FFUL;
    NRF_RADIO->BASE0     = (connectedContext.accessAddress << 8) & 0xFFFFFF00UL;
  }

  // Always use address 0 for sending and receiving
  NRF_RADIO->TXADDRESS = 0x00;          // Use logical address 0 (prefix0 + base0) = 0x8E89BED6 when transmitting
  NRF_RADIO->RXADDRESSES = 0x01;        // Enable reception on logical address 0 (PREFIX0 + BASE0)

	// CRC Format
  NRF_RADIO->CRCCNF  = (RADIO_CRCCNF_LEN_Three << RADIO_CRCCNF_LEN_Pos) |
                       (RADIO_CRCCNF_SKIPADDR_Skip << RADIO_CRCCNF_SKIPADDR_Pos); // Skip Address when computing crc
  NRF_RADIO->CRCPOLY = 0x00065B;    // CRC polynomial function


  // Lock inter-frame spacing
  NRF_RADIO->TIFS = 150;
}

void setupWindow(uint8_t winSize, uint16_t winOffset, uint16_t interval,
                 uint16_t latency, uint16_t timeout, uint16_t instant)
{
  connectedContext.windowSize = winSize * 1250;
  connectedContext.winOffset = winOffset * 1250;
  connectedContext.newInterval = interval * 1250;
  connectedContext.newTimeout = timeout * 10000;
  connectedContext.instant = instant;
}

static inline void setupAdvertisingTx(int channel)
{
  setChannel(channel);

  NRF_RADIO->PACKETPTR = (uint32_t)advPacket;

  NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                      RADIO_SHORTS_END_DISABLE_Msk |
                      RADIO_SHORTS_DISABLED_RXEN_Msk;

  NRF_PPI->CHENSET = PPI_CHENSET_CH27_Msk;   // RADIO->EVENT_END -> TIMER0->TASKS_CAPTURE[2];

  NRF_PPI->CHENCLR = PPI_CHENCLR_CH22_Msk |  // TIMER0->COMPARE[1] -> RADIO_TASKS_DISABLE
                     PPI_CHENCLR_CH26_Msk;   // RADIO->EVENT_ADDRESS -> TIMER0->TASKS_CAPTURE[1]
}

static inline void setupAdvertisingRx()
{
  NRF_RADIO->PACKETPTR = (uint32_t)&advRxPacket;

  NRF_TIMER0->TASKS_CAPTURE[3] = 1UL;

  // Rx timeout is 201us after the last received packet (152us + amount of bits in preample and access address + 1)
  NRF_TIMER0->EVENTS_COMPARE[1] = 0UL;
  NRF_TIMER0->CC[1] = NRF_TIMER0->CC[2]+201;

  NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                      RADIO_SHORTS_END_DISABLE_Msk |
                      RADIO_SHORTS_DISABLED_TXEN_Msk;

  NRF_PPI->CHENSET = PPI_CHENSET_CH22_Msk |  // TIMER0->COMPARE[1] -> RADIO_TASKS_DISABLE
                     PPI_CHENSET_CH26_Msk;   // RADIO->EVENT_ADDRESS -> TIMER0->TASKS_CAPTURE[1]

  NRF_PPI->CHENCLR = PPI_CHENCLR_CH27_Msk;   //  RADIO->EVENT_END -> TIMER0->TASKS_CAPTURE[2];
}

// If first is true, setup for transferring the next first connection event
static inline void setupConnRx(bool first)
{
  //NRF_RADIO->PACKETPTR = (uint32_t)&dataRxPacket;

  NRF_TIMER0->EVENTS_COMPARE[1] = 0UL;
  NRF_TIMER0->CC[1] = 0;//TODO: Calculate timeout

  NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                      RADIO_SHORTS_END_DISABLE_Msk |
                      RADIO_SHORTS_DISABLED_TXEN_Msk;

  NRF_PPI->CHENSET = PPI_CHENSET_CH22_Msk |  // TIMER0->COMPARE[1] -> RADIO_TASKS_DISABLE
                     PPI_CHENSET_CH26_Msk;   // RADIO->EVENT_ADDRESS -> TIMER0->TASKS_CAPTURE[1]

  NRF_PPI->CHENCLR = PPI_CHENCLR_CH27_Msk;   //  RADIO->EVENT_END -> TIMER0->TASKS_CAPTURE[2];
}

// This is a slave stack: the TX transfers is never first
static inline void setupConnTx()
{

}

static int cc[3][4];

void RADIO_IRQHandler(void)
{
  static bool connecting = false;
	NVIC_ClearPendingIRQ(RADIO_IRQn);

	uartPuts("Radio int. ");

	if (NRF_RADIO->EVENTS_DISABLED) {
		NRF_RADIO->EVENTS_DISABLED = 0ul;
		if (llState == state_advertising) {
      switch (advState) {
      case adv_tx:
        setupAdvertisingRx();
        NRF_PPI->CHENSET = PPI_CHENSET_CH0_Msk;
        uartPuts("TX   ");
        uartPuts(advChannel==37?"37":(advChannel==38?"38":"39"));
        advState = adv_rx;
        //if (advChannel == 38) __asm__("bkpt #0");
        cc[advChannel-37][0] = NRF_TIMER0->CC[3] - NRF_TIMER0->CC[2];
        cc[advChannel-37][1] = NRF_TIMER0->CC[1] - NRF_TIMER0->CC[2];
        NRF_TIMER0->CC[0] = NRF_TIMER0->CC[2];
        break;
      case adv_rx:
        NRF_PPI->CHENCLR = PPI_CHENCLR_CH0_Msk;
        if (NRF_TIMER0->EVENTS_COMPARE[1] == 0) {
          // TODO: Add random timing to advInterval
          NRF_RTC0->CC[0] = advInterval; //RTC0 has been cleared, reset CC0

          if (advRxPacket.header.type==scan_req) {
            NRF_RADIO->PACKETPTR = (uint32_t)advTxPacket;
            NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                                RADIO_SHORTS_END_DISABLE_Msk;
          } else if (advRxPacket.header.type==connect_req) {
            NRF_RADIO->SHORTS = 0UL;
            NRF_RADIO->TASKS_DISABLE = 1UL;

            // Next state will initiate connection
            connecting = true;
          } else {
            NRF_RADIO->SHORTS = 0UL;
            NRF_RADIO->TASKS_DISABLE = 1UL;
          }
          NRF_TIMER0->TASKS_CAPTURE[3] = 1UL;
          cc[advChannel-37][2] = NRF_TIMER0->CC[3] - NRF_TIMER0->CC[0];

          uartPuts("RX   ");
          uartPuts(advChannel==37?"37":(advChannel==38?"38":"39"));
          uartPuts(" Packet received Type: ");
          uartPuts(advRxPacket.header.type==adv_ind?"ADV_IND":(advRxPacket.header.type==connect_req?"CONNECT_REQ":(advRxPacket.header.type==scan_req?"SCAN_REQ":"ND")));
        } else {
          NRF_RADIO->SHORTS = 0UL;
          NRF_RADIO->TASKS_DISABLE = 1UL;

          uartPuts("RX   ");
          uartPuts(advChannel==37?"37":(advChannel==38?"38":"39"));
        }
        NRF_TIMER0->EVENTS_COMPARE[1] = 0UL;
        NRF_TIMER0->TASKS_CAPTURE[3] = 1UL;
        cc[advChannel-37][2] = NRF_TIMER0->CC[3] - NRF_TIMER0->CC[0];


        advState = adv_txscan;
        break;
      case adv_txscan:
        if (connecting) {
          connecting = false;
          advState = adv_idle;
          obleLinkStopAdvertising();

          memset(&connectedContext, 0, sizeof(connectedContext));

          // Fill up context with information from connection request packet
          connectedContext.accessAddress = advRxPacket.connect_req.aa;
          connectedContext.crcInit = advRxPacket.connect_req.crcInit;
          connectedContext.hop = advRxPacket.connect_req.hop;

          connectedContext.winOffset = advRxPacket.connect_req.winOffset*1250;
          connectedContext.windowSize = advRxPacket.connect_req.winSize*1250;

          connectedContext.newInterval = advRxPacket.connect_req.interval*1250;
          connectedContext.newLatency = advRxPacket.connect_req.latency;
          connectedContext.newTimeout = advRxPacket.connect_req.timeout*10000;

          setChM(advRxPacket.connect_req.chM);

          {
            char buff[10];
            int i;

            uartPuts("chM: ");
            for (i=0; i<5; i++) {
              sprintf(buff, "%X ", connectedContext.chM[i]);
              uartPuts(buff);
            }
          }

          connectedContext.masterSca = advRxPacket.connect_req.sca;

          // Default connection parameter
          connectedContext.interval = 1250;
          connectedContext.latency = 0;
          connectedContext.timeout = 0;
          connectedContext.connEventCounter = 0;

          connectedContext.dataRx = &dataDummyPacket;
          connectedContext.dataLastTx = &dataDummyPacket;

          radioInit(false);
          connectedContext.unmappedChannel += connectedContext.hop;
          connectedContext.unmappedChannel %= 37;
          setChannel(connectedContext.unmappedChannel);

          connectedContext.sn = 1;
          connectedContext.nesn = 0;

          llState = state_connected;

          NRF_TIMER0->TASKS_STOP = 1ul;
          NRF_TIMER0->TASKS_CLEAR = 1ul;

          NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                              RADIO_SHORTS_END_DISABLE_Msk |
                              RADIO_SHORTS_DISABLED_TXEN_Msk;
          NRF_RADIO->PACKETPTR = (uint32_t)&dataDummyPacket;

          NRF_PPI->CHENSET = PPI_CHENSET_CH29_Msk | // RTC0->CC[0] -> RADIO->RXEN
                             PPI_CHENSET_CH31_Msk | // RTC0->CC[0] -> TIMER0->START
                             PPI_CHENSET_CH1_Msk  | // RADIO->ADDRESS -> RTC0->CLEAR
                             PPI_CHENSET_CH26_Msk | // RADIO->ADDRESS -> TIMER0->CC[1]
                             PPI_CHENSET_CH22_Msk;  // TIMER0->CC[1] -> RADIO->DISABLE
          NRF_RTC0->CC[0] = US2RTC(1250+connectedContext.winOffset-400);
          NRF_TIMER0->EVENTS_COMPARE[1] = 0UL;
          NRF_TIMER0->CC[1] = connectedContext.windowSize + 800;
          NRF_RADIO->EVENTS_ADDRESS = 0UL;

          if (connectedContext.windowSize) {
            connectedContext.windowSize = 0;
            connectedContext.interval = connectedContext.newInterval;
            connectedContext.latency = connectedContext.newLatency;
            connectedContext.timeout = connectedContext.newTimeout;
          }

          dataState = data_rx;

          // Reseting higher layer protocols for the new connection
          obleLlcNewConnection();

        } else {
          NRF_TIMER0->TASKS_CAPTURE[3] = 1UL;
          cc[advChannel-37][3] = NRF_TIMER0->CC[3] - NRF_TIMER0->CC[0];
          uartPuts("SCAN ");
          uartPuts(advChannel==37?"37":(advChannel==38?"38":"39"));
          if (++advChannel == 40) {
            advChannel = 37;
            setupAdvertisingTx(advChannel);  //Prepare for next run
            nrf_gpio_pin_set(21);
          } else {
            setupAdvertisingTx(advChannel);
            NRF_RADIO->TASKS_TXEN = 1UL; // Send adv on next channel
          }
          advState = adv_tx;
        }
        break;
      default:
        break;
      }
		} else if (llState == state_connected) {
		  if (dataState == data_rx) {
		    if (NRF_RADIO->EVENTS_ADDRESS) {
		      struct dataPacket_s *pk;
		      bool dataReceived = false;

		      // Configuring radio to TX, the radio is currently entering TX mode so we do it first
		      if (connectedContext.dataRx && (connectedContext.dataRx->header.nesn == connectedContext.sn)) {
		        // Retry last sent packet
		        pk = connectedContext.dataLastTx;
		      } else {
		        // Send a new packet
		        connectedContext.sn++;
		        pk = queue_get(&dataTxQueue);

		        if (!pk) {
		          pk = &dataDummyTxPacket;
		        }
		      }
          NRF_RADIO->PACKETPTR = (uint32_t)pk;

          if (pk == &dataDummyTxPacket) {
            // Sending an empty packet
            pk->header.length = 0;
            pk->header.llid = 1;
          }
          pk->header.sn = connectedContext.sn;

          if (NRF_RADIO->CRCSTATUS &&
              (connectedContext.dataRx != &dataDummyPacket) &&
              (connectedContext.dataRx->header.sn == connectedContext.nesn)) {
            connectedContext.nesn++; //Acking the received packet
            dataReceived = true;
          }
          pk->header.nesn = connectedContext.nesn;

          NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                              RADIO_SHORTS_END_DISABLE_Msk;

          NRF_PPI->CHENCLR = PPI_CHENSET_CH1_Msk  | // RADIO->ADDRESS -> RTC0->CLEAR
                             PPI_CHENSET_CH22_Msk;  // TIMER0->CC[1] -> RADIO->DISABLE

          if ((connectedContext.dataLastTx != &dataDummyTxPacket) &&
              (connectedContext.dataLastTx != pk)) {
            queue_put(&dataParkingQueue, connectedContext.dataLastTx);
          }
          connectedContext.dataLastTx = pk;

          // Handling the RXed packet
          if (dataReceived) {
            queue_put(&dataRxQueue, connectedContext.dataRx);
          }

          uartPuts("RD OK");
          if (connectedContext.dataRx->header.llid == 1)
            uartPuts(" EMPTY");
          else if (connectedContext.dataRx->header.llid == 2)
            uartPuts(" L2CAP");
          else
            uartPuts(" LLC");

          if (dataReceived)
            uartPuts(" R");
          else
            uartPuts(" N");

          if (connectedContext.dataRx->header.sn == 0)
            uartPuts(" 0");
          else
            uartPuts(" 1");

          if (dataReceived || (connectedContext.dataRx == &dataDummyPacket)) {
            // Setup a new packet to be RXed
            connectedContext.dataRx = queue_get(&dataParkingQueue);
            if (connectedContext.dataRx == NULL) {
              connectedContext.dataRx = &dataDummyPacket;
              //__asm__("bkpt #0");
            }
          }

          connectedContext.timeSinceAnchor = 0;
          nrf_gpio_pin_clear(22);
          nrf_gpio_pin_set(21);

          if (connectedContext.dataRx == &dataDummyPacket)
            uartPuts(" DUMMYRX");


		    } else {
		      NRF_RADIO->SHORTS = 0;
		      NRF_RADIO->TASKS_DISABLE = 1UL;

		      connectedContext.timeSinceAnchor += connectedContext.interval;
		      nrf_gpio_pin_clear(21);
		      uartPuts("RD fail");
		    }

        char ceBuffer[20];
        sprintf(ceBuffer, " %d", (int)connectedContext.connEventCounter);
        ceBuffer[19] = 0;
        uartPuts(ceBuffer);

		    NRF_TIMER0->TASKS_CAPTURE[3] =1UL;

        NRF_TIMER0->TASKS_STOP = 1ul;
        NRF_TIMER0->TASKS_CLEAR = 1ul;

		    dataState = data_tx;
		  } else if (dataState == data_tx) {
		    struct dataPacket_s *pk;

        // End of this connection event (todo: handle connection events with
        // more than 1 exchange ...), increment the even counter
        connectedContext.connEventCounter++;

		    bool txed = NRF_RADIO->SHORTS != 0;
        connectedContext.unmappedChannel += connectedContext.hop;
        connectedContext.unmappedChannel %= 37;
        setChannel(connectedContext.unmappedChannel);

        NRF_RADIO->SHORTS = RADIO_SHORTS_READY_START_Msk |
                            RADIO_SHORTS_END_DISABLE_Msk |
                            RADIO_SHORTS_DISABLED_TXEN_Msk;
        NRF_RADIO->PACKETPTR = (uint32_t)connectedContext.dataRx;

        NRF_PPI->CHENSET = PPI_CHENSET_CH29_Msk | // RTC0->CC[0] -> RADIO->RXEN
                           PPI_CHENSET_CH31_Msk | // RTC0->CC[0] -> TIMER0->START
                           PPI_CHENSET_CH1_Msk  | // RADIO->ADDRESS -> RTC0->CLEAR
                           PPI_CHENSET_CH22_Msk;  // TIMER0->CC[1] -> RADIO->DISABLE

        NRF_TIMER0->TASKS_STOP = 1UL;
        NRF_TIMER0->TASKS_CLEAR = 1UL;

        if (connectedContext.instant == connectedContext.connEventCounter) {
          NRF_RTC0->CC[0] = US2RTC(connectedContext.timeSinceAnchor + connectedContext.interval + connectedContext.winOffset-400);
          // NRF_TIMER0->EVENTS_COMPARE[1] = 0UL;
          NRF_TIMER0->CC[1] = connectedContext.windowSize + 800;
          NRF_RADIO->EVENTS_ADDRESS = 0UL;

          connectedContext.instant = -1;
          connectedContext.windowSize = 0;
          connectedContext.interval = connectedContext.newInterval;
          connectedContext.latency = connectedContext.newLatency;
          connectedContext.timeout = connectedContext.newTimeout;
        } else {
          NRF_RTC0->CC[0] = US2RTC(connectedContext.timeSinceAnchor + (connectedContext.interval-400));
          NRF_TIMER0->CC[1] = 800;
          NRF_RADIO->EVENTS_ADDRESS = 0UL;
        }

        //NRF_RTC0->INTENCLR = RTC_INTENCLR_COMPARE0_Msk;

        dataState = data_rx;
        if (txed) {
          uartPuts("TD ");
        } else {
          uartPuts("PREP ");
        }
        {
          char chanelstr[5];
          sprintf(chanelstr, "%d", connectedContext.unmappedChannel);
          uartPuts(chanelstr);
        }
        nrf_gpio_pin_set(22);


        //Handling packets
        while ((pk = queue_get(&dataRxQueue)) != NULL) {
          if (pk->header.llid == 3) {
            if (obleLlcPacketRx(pk)) {
              queue_put(&dataTxQueue, pk);
            } else {
              queue_put(&dataParkingQueue, pk);
            }
          } else if (pk->header.llid == 2) {
            if (obleL2capPacketRx(pk)) {
              queue_put(&dataTxQueue, pk);
            } else {
              queue_put(&dataParkingQueue, pk);
            }
          } else {
            queue_put(&dataParkingQueue, pk);
          }
        }
		  }

      if(connectedContext.timeSinceAnchor > connectedContext.timeout) {
        uartPuts("Timeout, disconnecting!\r\n");
        obleLinkStopConnection();
      }
		  //__asm__("bkpt #0");
		}
	}

	uartPuts("\r\n");
}

void RTC0_IRQHandler(void)
{
  char delaystr[6];
  int delay;

  NVIC_ClearPendingIRQ(RTC0_IRQn);
  uartPuts(".\r\n");

  //uartPuts("RTC0 Int ");

  if (NRF_RTC0->EVENTS_COMPARE[0]) {
    NRF_RTC0->EVENTS_COMPARE[0] = 0;
    if (llState == state_advertising) {
      // When advertising RTC0->CC0 mark the beginning of an advertising event
      NRF_RADIO->TASKS_TXEN = 1UL;

      // Add random delay of up to 10ms to advertisement interval
      delay = rand()%32;
      sprintf(delaystr, "%d\r\n", delay);
      uartPuts(delaystr);

      NRF_RTC0->CC[0] += advInterval + delay;

      nrf_gpio_pin_clear(21);
    } else if (llState == state_connected) {
      // Nothing done here, all done in the Radio interrupt!
      nrf_gpio_pin_set(21);
      NRF_RADIO->TASKS_RXEN = 1UL;
      NRF_TIMER0->TASKS_START = 1UL;
    }
  }

  if (NRF_RTC0->EVENTS_COMPARE[1]) {
    NRF_RTC0->EVENTS_COMPARE[1] = 0;

    //nrf_gpio_pin_set(21);
  }

  return;
}

static void obleInit()
{
	// Start and reset Radio
  NRF_RADIO->POWER = 1UL;

	// Timer0: 1us resolution, Using it as a free-rolling timer
	NRF_TIMER0->PRESCALER = 4;
	NRF_TIMER0->BITMODE = TIMER_BITMODE_BITMODE_24Bit;
	NRF_TIMER0->TASKS_START = 1;

	// Enable Radio Disabled interrupt
	NRF_RADIO->INTENSET = RADIO_INTENSET_DISABLED_Msk;
	NVIC_EnableIRQ(RADIO_IRQn);

	NRF_RTC0->EVTENSET = 0xFFFFFFFF;

	// Setup PPI channel 0 from RADIO->EVENTS_END to RTC0->TASKS_CLEAR
	NRF_PPI->CH[0].EEP = (uint32_t)&NRF_RADIO->EVENTS_END;
	NRF_PPI->CH[0].TEP = (uint32_t)&NRF_RTC0->TASKS_CLEAR;

	// Setup PPI channel 1 from RADIO->EVENTS_ADDRESS to RTC0->TASKS_CLEAR
	NRF_PPI->CH[1].EEP = (uint32_t)&NRF_RADIO->EVENTS_ADDRESS;
	NRF_PPI->CH[1].TEP = (uint32_t)&NRF_RTC0->TASKS_CLEAR;
}

static inline void setupAdvertisingTimer()
{
  NRF_PPI->CHENSET = PPI_CHENSET_CH28_Msk; // rtc0->event_compare[0] -> radio->txen

  NRF_RTC0->PRESCALER = 0;
  NRF_RTC0->CC[0] = advInterval;
  NRF_RTC0->TASKS_CLEAR = 1UL;
  NRF_RTC0->TASKS_START = 1UL;

  NRF_RTC0->INTENSET = RTC_INTENSET_COMPARE0_Msk;
  NVIC_SetPriority(RTC0_IRQn, 2);
  NVIC_EnableIRQ(RTC0_IRQn);

  //NRF_PPI->CHENSET = PPI_CHENSET_CH30_Msk; //rtc0->compare[0] -> timer0-tasks_clear
}

bool obleLinkStartAdvertising()
{
  const static uint8_t chMAllActive[5] = {0xff, 0xff, 0xff, 0xff, 0xff};

  if (llState != state_standby) {
    return false;
  }

  setChM(chMAllActive);

  advState = adv_tx;

  obleInit();
  radioInit(true);         //Init radio for advertising PDU
  advChannel = 37;
  setupAdvertisingTx(37);
  setupAdvertisingTimer();



  llState = state_advertising;

  return true;
}

bool obleLinkStopAdvertising()
{
  if (llState != state_advertising) {
    return false;
  }

  NRF_PPI->CHENCLR = PPI_CHENCLR_CH28_Msk; // rtc0->event_compare[0] -> radio->txen

  //NRF_RTC0->TASKS_STOP = 1UL;
  //NRF_RTC0->TASKS_CLEAR = 1UL;

  return true;
}

bool obleLinkStopConnection()
{
  if (llState != state_connected) {
    return false;
  }

  NRF_RADIO->SHORTS = 0;

  NRF_RTC0->TASKS_STOP = 1UL;
  NRF_TIMER0->TASKS_STOP = 1UL;
  NRF_RADIO->TASKS_DISABLE = 1UL;

  NRF_PPI->CHENCLR = PPI_CHENCLR_CH29_Msk | // RTC0->CC[0] -> RADIO->RXEN
                     PPI_CHENCLR_CH31_Msk | // RTC0->CC[0] -> TIMER0->START
                     PPI_CHENCLR_CH1_Msk  | // RADIO->ADDRESS -> RTC0->CLEAR
                     PPI_CHENCLR_CH26_Msk | // RADIO->ADDRESS -> TIMER0->CC[1]
                     PPI_CHENCLR_CH22_Msk;  // TIMER0->CC[1] -> RADIO->DISABLE


  llState = state_standby;
  return true;
}

#define NPACKET 4

static struct dataPacket_s packets[NPACKET];

void obleLinkInit()
{
  int i;
  struct dataPacket_s *pk;

  // Initializing the unused packet queue
  dataParkingQueue = &packets[0];
  pk = &packets[0];

  for (i=1; i<NPACKET; i++) {
    pk->next = &packets[i];
    pk = pk->next;
  }

  pk->next = NULL;

  dataTxQueue = NULL;
  dataRxQueue = NULL;
}

obleLlState_t obleGetState()
{
  return llState;
}

void obleLinkSetAdvPackets(struct advPacket_s * newAdvPacket, struct advPacket_s * newScanPacket)
{
  advPacket = newAdvPacket;
  advTxPacket = newScanPacket;
}

bool obleLinkAdvSetAdvPeriod(uint32_t period_ms)
{
  if (period_ms > 32000) {
    return false;
  }

  advInterval = US2RTC(period_ms * 1000);
  return true;
}
