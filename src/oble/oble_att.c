#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <uart.h>

#include <oble.h>
#include "ll_priv.h"

#include <att_table.h>
#include "att_pdu.h"

struct attPdu_s {
  uint16_t l2capLength;
  uint16_t l2capType;
  uint8_t op;
  union {
    struct {
      uint16_t mtu;
    } __attribute__((packed)) exchange_mtu;
    struct {
      uint8_t op;
      uint16_t handle;
      uint8_t error;
    } __attribute__((packed)) error;
    struct {
      uint16_t startingHandle;
      uint16_t endingHandle;
    } __attribute__((packed)) find_information_req;
    struct {
      uint8_t format;
      struct {
        uint16_t handle;
        uint16_t uuid16;
      } __attribute__((packed)) data[5];
    } __attribute__((packed)) find_information_res16;
    struct {
      uint8_t format;
      uint16_t handle;
      char uuid[16];
    } __attribute__((packed)) find_information_res128;
    struct {
      uint16_t startingHandle;
      uint16_t endingHandle;
      uint16_t attributeType;
      uint8_t attributeValue[16];
    } __attribute__((packed)) find_by_type_value_req;
    struct {
      struct {
        uint16_t handle;
        uint16_t endHandle;
      } data[5];
    } __attribute__((packed)) find_by_type_value_res;
    struct {
      uint16_t startingHandle;
      uint16_t endingHandle;
      union {
        uint16_t attributeType;
        char attributeType128[16];
      } __attribute__((packed));
    } __attribute__((packed)) read_by_type_req;
    struct {
      uint8_t length;
      char list[ATT_MTU-1];
    } __attribute__((packed)) read_by_type_res;
    struct {
      uint16_t handle;
    } __attribute__((packed)) read_req;
    struct {
      char value[ATT_MTU-1];
    } __attribute__((packed)) read_res;
    struct {
          uint16_t startingHandle;
          uint16_t endingHandle;
          union {
            uint16_t type16;
            char type128[16];
          };
        } __attribute__((packed)) read_by_group_type_req;
    struct {
      uint8_t length;
      union {
        struct {
          uint16_t handle;
          uint16_t endHandle;
          char value[2];
        } __attribute__((packed)) list16[3];;
        struct {
          uint16_t handle;
          uint16_t endHandle;
          char value[16];
        }__attribute__((packed)) list128[1];
        char datalist[ATT_MTU-2];
      };
    } __attribute__((packed)) read_by_group_type_res;
    struct {
      uint16_t handle;
      char value[ATT_MTU-3];
    } __attribute__((packed)) write_req;
  };
} __attribute__((packed));

bool obleAttPacketRx(struct dataPacket_s *packet)
{
  struct attPdu_s *pdu = (void*)packet->payload;
  static char cachedValue[ATT_MTU];

  uartPuts(" ATT");

  switch (pdu->op) {
    case ATT_OP_EXCHANGE_MTU_REQ:
      pdu->exchange_mtu.mtu = 27;
      pdu->op = ATT_OP_EXCHANGE_MTU_RES;
      return true;
      break;
    case ATT_OP_FIND_INFORMATION_REQ:
    {
      int start = pdu->find_information_req.startingHandle;
      int end = pdu->find_information_req.endingHandle;
      int i=0;
      struct attElement_s * element;
      int length = 0;

      for (i=0; i<5; i++) {
        element = obleAttTableGetNextElement(start, end);
        if (!element) break;

        if (uuidIsUuid16(element->uuid)) {
          pdu->find_information_res16.format = 0x01;
          pdu->find_information_res16.data[i].handle = element->handle;
          pdu->find_information_res16.data[i].uuid16 = uuidGetUuid16(element->uuid);
          length += 4;
          start = element->handle+1;
        } else if (i == 0) {
          pdu->find_information_res16.format = 0x02;
          pdu->find_information_res128.handle = element->handle;
          uuidGetUuid128(pdu->find_information_res128.uuid, element->uuid);
          length += 18;
          i++;
          break;
        } else {
          break;
        }
      }

      if (i>0) {
        pdu->op = ATT_OP_FIND_INFORMATION_RES;
        pdu->l2capLength = 2 + length;
        packet->header.length = pdu->l2capLength+4;
      } else {
        pdu->error.op = pdu->op;
        pdu->error.handle = start;
        pdu->error.error = ATT_ERROR_ATTRIBUTE_NOT_FOUND;
        pdu->op = ATT_OP_ERROR_RES;
        pdu->l2capLength = sizeof(pdu->error)+1;
        packet->header.length = pdu->l2capLength+4;
      }

      return true;
      break;
    }
    case ATT_OP_FIND_BY_TYPE_VALUE_REQ:
    {
      int i;
      int start = pdu->find_by_type_value_req.startingHandle;
      int end = pdu->find_by_type_value_req.endingHandle;
      uuid_t type = uuidFromUuid16(pdu->find_by_type_value_req.attributeType);
      int length = pdu->l2capLength-7;
      memcpy(cachedValue, pdu->find_by_type_value_req.attributeValue, length);
      struct attElement_s *element;
      int error;

      for (i=0; i<4; i++) {
        uint16_t handle;
        uint16_t groupEnd;

        element = obleAttTableGetElementByTypeValue(start, end, type, length, cachedValue);
        if (!element) {
          break;
        }
        error = obleAttTableGetGroupByType(element->handle, end, type, &handle, &groupEnd);
        if (error) {
          break;
        }

        pdu->find_by_type_value_res.data[i].handle = handle;
        pdu->find_by_type_value_res.data[i].endHandle = groupEnd;

        start = groupEnd+1;
      }

      if (i>0) {
        pdu->op = ATT_OP_FIND_BY_TYPE_VALUE_RES;
        pdu->l2capLength = 1 + (4*i);
        packet->header.length = pdu->l2capLength+4;
      } else {
        pdu->error.op = pdu->op;
        pdu->error.handle = start;
        pdu->error.error = error?error:ATT_ERROR_ATTRIBUTE_NOT_FOUND;
        pdu->op = ATT_OP_ERROR_RES;
        pdu->l2capLength = sizeof(pdu->error)+1;
        packet->header.length = pdu->l2capLength+4;
      }

      return true;
      break;
    }
    case ATT_OP_READ_BY_TYPE_REQ:
    {
      int i=0;
      int start = pdu->read_by_type_req.startingHandle;
      int end = pdu->read_by_type_req.endingHandle;
      uuid_t type;
      struct attElement_s *element;
      int elementLength;

      if (pdu->l2capLength > 7) {
        type = uuidFromUuid128(pdu->read_by_type_req.attributeType128);
      } else {
        type = uuidFromUuid16(pdu->read_by_type_req.attributeType);
      }

      while(i<(ATT_MTU-2)) {
        element = obleAttTableGetElementByType(start, end, type);

        if (element == NULL) break;

        start = element->handle+1;

        element->read(element->context, cachedValue, &elementLength, ATT_MTU-2);

        if (i == 0) {
          pdu->read_by_type_res.length = elementLength;
        } else if (elementLength != pdu->read_by_type_res.length) {
          break;
        }

        if ((elementLength+i+2)>(ATT_MTU-2))
          break;

        memcpy(&pdu->read_by_type_res.list[i], &element->handle, 2);
        i+=2;
        memcpy(&pdu->read_by_type_res.list[i], cachedValue, elementLength);
        i+=elementLength;
      }

      if (i>0) {
        pdu->read_by_type_res.length += 2;

        pdu->op = ATT_OP_READ_BY_TYPE_RES;
        pdu->l2capLength = 2 + i;
        packet->header.length = pdu->l2capLength+4;
      } else {
        pdu->error.op = pdu->op;
        pdu->error.handle = start;
        pdu->error.error = ATT_ERROR_ATTRIBUTE_NOT_FOUND;
        pdu->op = ATT_OP_ERROR_RES;
        pdu->l2capLength = sizeof(pdu->error)+1;
        packet->header.length = pdu->l2capLength+4;
      }
      return true;
      break;
    }
    case ATT_OP_READ_REQ:
    {
      uint16_t handle = pdu->read_req.handle;
      struct attElement_s *element;
      int length;

      element = obleAttTableGetElement(handle);

      if (element) {
        element->read(element->context, cachedValue, &length, ATT_MTU-1);

        memcpy(pdu->read_res.value, cachedValue, length);

        pdu->op = ATT_OP_READ_RES;
        pdu->l2capLength = 1+length;
        packet->header.length = pdu->l2capLength+4;
      } else {
        pdu->error.op = pdu->op;
        pdu->error.handle = handle;
        pdu->error.error = ATT_ERROR_INVALID_HANDLE;
        pdu->op = ATT_OP_ERROR_RES;
        pdu->l2capLength = sizeof(pdu->error)+1;
        packet->header.length = pdu->l2capLength+4;
      }
      return true;
      break;
    }
    case ATT_OP_READ_BY_GROUP_TYPE_REQ:
    {
      uartPuts("\r\nread by group\r\n");
      int start = pdu->read_by_group_type_req.startingHandle;
      int end = pdu->read_by_group_type_req.endingHandle;
      uuid_t type;
      int valueLength = -1; // Hold the element length
      int currentPduLength = 0; // Hold how much we filled the PDU so far
      int error = ATT_ERROR_UNLIKELY_ERROR;

      if (pdu->l2capLength > 7) {
        type = uuidFromUuid128(pdu->read_by_group_type_req.type128);
      } else {
        type = uuidFromUuid16(pdu->read_by_group_type_req.type16);
      }

      while ((2+currentPduLength + (valueLength+4)) < ATT_MTU) {
        uint16_t groupHandle;
        uint16_t groupEnd;
        struct attElement_s *element;
        int elementLength;

        error = obleAttTableGetGroupByType(start, end, type, &groupHandle, &groupEnd);
        if (error) {
          break;
        }

        element = obleAttTableGetElement(groupHandle);
        if (!element) {
          error = ATT_ERROR_UNLIKELY_ERROR;
          break;
        }

        error = element->read(element->context, cachedValue, &elementLength, ATT_MTU-6);
        if ((valueLength >= 0) && (elementLength != valueLength)) {
          break;
        }
        valueLength = elementLength;

        memcpy(&pdu->read_by_group_type_res.datalist[currentPduLength], &groupHandle, 2);
        memcpy(&pdu->read_by_group_type_res.datalist[currentPduLength+2], &groupEnd, 2);
        memcpy(&pdu->read_by_group_type_res.datalist[currentPduLength+4], cachedValue, valueLength);

        currentPduLength += (valueLength+4);
        start = groupEnd+1;
      }

      if (currentPduLength > 0) {
        pdu->op = ATT_OP_READ_BY_GROUP_TYPE_RES;
        pdu->read_by_group_type_res.length = valueLength+4;
        pdu->l2capLength = currentPduLength+2;
        packet->header.length = pdu->l2capLength+4;
      } else {
        uartPuts("read by group empty?\r\n");
        if (error == 0) {
          error = ATT_ERROR_UNLIKELY_ERROR;
        }
        pdu->error.op = ATT_OP_READ_BY_GROUP_TYPE_REQ;
        pdu->error.handle = start;
        pdu->error.error = error;
        pdu->op = ATT_OP_ERROR_RES;
        pdu->l2capLength = sizeof(pdu->error)+1;
        packet->header.length = pdu->l2capLength+4;
      }

      return true;
      break;
    }
    case ATT_OP_WRITE_REQ:
    case ATT_OP_WRITE_CMD:
    {
      uint16_t handle = pdu->write_req.handle;
      struct attElement_s *element = obleAttTableGetElement(handle);
      int res;

      if (element) {
        res = element->write(element->context, pdu->write_req.value, pdu->l2capLength - 3);
      } else {
        res = ATT_ERROR_ATTRIBUTE_NOT_FOUND;
      }

      if (pdu->op == ATT_OP_WRITE_REQ) {
        if (res == 0) {
          // Send a write response. The write response does not contain any data
          pdu->op = ATT_OP_WRITE_RES;
          pdu->l2capLength = 1;
          packet->header.length = pdu->l2capLength+4;
        } else {
          // Send back an error, the error code is given by the higher level protocol
          pdu->op = ATT_OP_ERROR_RES;
          pdu->error.op = ATT_OP_WRITE_REQ;
          pdu->error.error = res;
          pdu->error.handle = handle;
          pdu->l2capLength = sizeof(pdu->error)+1;
          packet->header.length = pdu->l2capLength+4;
        }
        return true;
      }
      break;
    }
    default:
      {char str[30]; sprintf(str, " UNKNOWN OP %d", pdu->op); uartPuts(str);}
      pdu->error.op = pdu->op;
      pdu->error.handle = 0;
      pdu->error.error = ATT_ERROR_REQUEST_NOT_SUPPORTED;
      pdu->op = ATT_OP_ERROR_RES;
      pdu->l2capLength = sizeof(pdu->error)+1;
      packet->header.length = pdu->l2capLength+4;
      return true;
      break;
  }

  return false;
}
