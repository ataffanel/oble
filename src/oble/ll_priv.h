/* Low lever control of the Link Layer used by llc */
#ifndef __LL_PRIV_H__
#define __LL_PRIV_H__

#include <stdint.h>
#include <stdbool.h>
#include "oble.h"

void setupWindow(uint8_t winSize, uint16_t winOffset, uint16_t interval,
                 uint16_t latency, uint16_t timeout, uint16_t instant);

void setChannelMap(uint16_t instant, char *newmap);

void obleLlcNewConnection();

bool obleLlcPacketRx(struct dataPacket_s *packet);

bool obleL2capPacketRx(struct dataPacket_s *packet);

bool obleAttPacketRx(struct dataPacket_s *packet);

bool obleLinkStopConnection();

#endif //__LL_PRIV_H__
