#ifndef __OBLE_H__
#define __OBLE_H__

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include <oble/uuid.h>

// Packet format for advertisement and data
enum advType {adv_ind = 0, adv_direct_ind = 1, adv_nonconn_ind = 2, scan_req=3, scan_rsp=4, connect_req=5, adv_scan_ind=6};

#define OBLE_ADV_PAYLOAD_LENGTH 27

struct advPacket_s {
  struct {
    uint8_t type:4;
    uint8_t rfu0:2;
    uint8_t txAddr:1;
    uint8_t rxAddr:1;
    uint8_t length;
    uint8_t rfu1;
  } __attribute__((packed)) header;

  union {
    uint8_t payload[36];
    struct {
      uint8_t initA[6];
      uint8_t advA[6];
      uint32_t aa;
      uint32_t crcInit:24;
      uint8_t winSize;
      uint16_t winOffset;
      uint16_t interval;
      uint16_t latency;
      uint16_t timeout;
      uint8_t chM[5];
      uint8_t hop:5;
      uint8_t sca:3;
    } __attribute__((packed)) connect_req;
  };
} __attribute__((packed));

struct dataPacket_s {
  struct {
    uint8_t llid:2;
    uint8_t nesn:1;
    uint8_t sn:1;
    uint8_t md:1;
    uint8_t rfu:3;
    uint8_t length;
  } __attribute__((packed)) header;
  uint8_t payload[OBLE_ADV_PAYLOAD_LENGTH];

  // Packet queue management
  struct dataPacket_s *next;
} __attribute__((packed));

typedef enum {state_standby, state_advertising, state_connected} obleLlState_t;

// General stack functions
obleLlState_t obleGetState();

// Link Layer
void obleLinkInit();

bool obleLinkStartAdvertising();
bool obleLinkStopAdvertising();

/**
 * Set advertisements packets.
 *
 * Two packets can be sent: The advertisement packet is sent at regular
 * interval, After sending the advertisement packet, the stack is listenning
 * for a scann request and if one is received the scann packet is sent. So a
 * passive scanner will only receive the adv packet and an active scanner can
 * get the scann packet.
 *
 * @param [in] advPacket Pointer to the advertisement packet. If NULL the stack
 *                       will not be able to start advertising
 * @param [in] scanPacket Pointer to the scan packet. If NULL, the stack will
 *                        ignore scann request
 */
void obleLinkSetAdvPackets(struct advPacket_s * advPacket, struct advPacket_s * scanPacket);

/**
 * Set advertisement period in ms.
 *
 * @param [in] period_ms Advertising period
 *
 * @note To be conform with the standard, the advertisement period should be a
 * multiple of 10ms
 */
bool obleLinkAdvSetAdvPeriod(uint32_t period_ms);


#endif
