/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie 2.0 pinout
 */
#ifndef __PINOUT_H__
#define __PINOUT_H__

#define UART_RTS_PIN 1
#define UART_TX_PIN 24
#define UART_CTS_PIN 2
#define UART_RX_PIN 25


#endif //__PINOUT_H__
