// Advertisement handling high-layer functions
// Advertisement are setup using a configuration opaque object. GAP

#ifndef __OBLE_ADV__
#define __OBLE_ADV__

#include "oble.h"
#include <stdbool.h>
#include <stdint.h>

// Opaque structure that contains the advertisement state
typedef struct obleAdv_s {
  struct advPacket_s advPacket;
  struct advPacket_s scanPacket;
  uint32_t period_ms;
  char address[6];
} obleAdv_t;

typedef enum obleAdvAddressType_e {
  publicAddress=0, randomAddress=1,
} obleAdvAddressType_t;

typedef enum obleAdvPacketType_e {
  advPacket, scanPacket, anyPacket
} obleAdvPacketType_t;

/**
 * Initialize an advertisement configuration object
 *
 * @param adv The advertisement object
 * @param [in] address The advertised address
 * @param [in] addressType The advertiser address type
 * @param [in] period_ms The advertisement period in milliseconds
 */
void obleAdvInitAdv(obleAdv_t *adv, const char address[6], obleAdvAddressType_t addressType, uint32_t period_ms);

/**
 * Append one GAP item to the advertisement packet.
 *
 * @param adv The advertisement object
 * @param [in] packetType packet to append data to. Either advPacket or scanPacket
 * @param [in] type The GAP value type
 * @param [in] value Pointer to the value
 * @param [in] length Length of the value
 * @return true if the item has been appended successfully, false if the packet
 *         does not have any space left
 */
bool obleAdvAppendItemToPacket(obleAdv_t *adv, obleAdvPacketType_t packetType, uint8_t type, char *value, int length);

/**
 * Append one GAP item to the advertisement packet.
 *
 * @param adv The advertisement object
 * @param [in] packetType packet to append data to. Either advPacket or scanPacket
 * @param [in] value Pointer to the data
 * @param [in] length Length of the data
 * @return true if the item has been appended successfully, false if the packet
 *         does not have any space left
 */
bool obleAdvAppendRawToPacket(obleAdv_t *adv, obleAdvPacketType_t packetType, void *value, int length);

int obleAdvGetPacketFreeSpace(obleAdv_t *adv, obleAdvPacketType_t packetType);

void* obleAdvGetPacketPayloadTail(obleAdv_t *adv, obleAdvPacketType_t packetType);

/**
 * Set active advertisement configuration in the stack
 *
 * @param adv The advertisement object
  */
void obleAdvSetActiveAdvertisement(obleAdv_t *adv);

#endif /* end of include guard: __OBLE_ADV__ */
