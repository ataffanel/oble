#ifndef __OBLE_GATT_H__
#define __OBLE_GATT_H__

#include <att_table.h>
#include <att_pdu.h>
#include <oble/uuid.h>

#define OBLE_GATT_PRIMARY_SERVICE 0x2800
#define OBLE_GATT_SECONDARY_SERVICE 0x2801
#define OBLE_GATT_INCLUDE 0x2802
#define OBLE_GATT_CHARACTERISTIC 0x2803
#define OBLE_GATT_CHARACTERISTIC_EXTENDED_PROPERTIES 0x2900
#define OBLE_GATT_CHARACTERISTIC_USER_DESCRIPTION 0x2901
#define OBLE_GATT_CHARACTERISTIC_CLIENT_CONFIGURATION 0x2902
#define OBLE_GATT_CHARACTERISTIC_SERVER_CONFIGURATION 0x2903
#define OBLE_GATT_CHARACTERISTIC_FORMAT 0x2904
#define OBLE_GATT_CHARACTERISTIC_AGREGATE_FORMAT 0x2905

struct obleGattChar_s;
struct obleGattInclude_s;

typedef struct obleGattService_s {
  struct attElement_s attribute;
  uuid_t uuid;
  bool primary;

  struct obleGattInclude_s *includes;
  struct obleGattChar_s *characteristics;

  struct obleGattService_s *next;
} obleGattService_t;

typedef struct obleGattInclude_s {
  struct attElement_s attribute;
  struct obleGattService_s *service;

  struct obleGattInclude_s *next;
} obleGattInclude_t;

typedef struct obleGattChar_s {
  struct attElement_s attribute;
  uuid_t uuid;
  uint8_t accessRight;

  struct attElement_s value;
  char valueBuffer[ATT_MTU];
  int valueLength;
  // Todo: add callback for read/write

  struct obleAttElement_s *descriptors;

  struct obleGattChar_s *next;
} obleGattChar_t;

void obleGattInit(char *localName);
void obleGattAddService(obleGattService_t *service);
void obleGattBuildTable();

void obleGattServiceInit(obleGattService_t *service, uuid_t uuid);
void obleGattServiceAddChar(obleGattService_t *service, obleGattChar_t *characteristic);

void obleGattCharInit(obleGattChar_t *characteristic, uuid_t uuid);
void obleGattCharSetValue(obleGattChar_t *characteristic, char *value, int lenght);

#endif /* end of include guard: __OBLE_GATT_H__ */
