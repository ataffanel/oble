// UUID handling
#ifndef __OBLE_UUID_H__
#define __OBLE_UUID_H__

#include <stdint.h>
#include <stdbool.h>

// TODO: Move this configuration in a configuration header
// Number of custom base UUID possible to register
#define UUID_BASETABLE_LENGTH 4

// An UUID is stored in an abstract type. Internally this is a uint32_t
// The 32bit int is separated in 2 16 bit field:
// - The low 16bit encodes the low 16bits of the first 32bits of the UUID
//   (similar as for the bluetooth 16bit uuid)
// - The high 16bit is an index in a table of base UUID to recontruct the full
//   UUID
// The base bluetooth UUID is index 0, this allows to convert bluetooth 16bit
// UUID to this format using a simple cast.
typedef uint32_t uuid_t;

#define UUID_INVALID 0xFFFF0000

// Conversion functions
#define uuidFromUuid16(UUID16) ( (uuid_t) UUID16 )
uuid_t uuidFromUuid16AndBase(int base, int uuid16);
uuid_t uuidFromUuid128(char* uuid128);
bool uuidGetUuid128(char *out, uuid_t uuid);
#define uuidGetUuid16(UUID) ( UUID & 0x0000FFFF )

// Comparison function, return false if any is invalid
bool uuidIsEqual(uuid_t uuid1, uuid_t uuid2);

// Base index handling
#define uuidGetBase(UUID) ( (UUID & 0xFFFF0000) >> 16 )

#define uuidIsUuid16(UUID) ( (UUID & 0xFFFF0000 ) == 0 )

/**
 * Register base UUID
 *
 * Returns the base index or -1 in case there is no more space left for new base
 */
int uuidRegisterBase(char * baseUuid);

#endif /* end of include guard: __OBLE_UUID_H__ */
