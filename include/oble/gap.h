#ifndef __OBLE_GAP_H__
#define __OBLE_GAP_H__

#include <stdbool.h>
#include <stdint.h>

#include <oble/uuid.h>

// GAP message type definition
// GAP EIR data type, see https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
#define GAP_FLAGS 0x01
#define GAP_INCOMPLETE_16BITS_UUID_LIST 0x02
#define GAP_COMPLET_16BITS_UUID_LIST 0x03
#define GAP_IHCOMPLET_32BITS_UUID_LIST 0x04
#define GAP_COMPLET_32BITS_UUID_LIST 0x05
#define GAP_INCOMPLET_128BITS_UUID_LIST 0x06
#define GAP_COMPLET_128BITS_UUID_LIST 0x07
#define GAP_SHORTEN_LOCAL_NAME 0x08
#define GAP_LOCAL_NAME 0x09
#define GAP_TX_POWER_LEVEL 0x0A
#define GAP_CLASS_OF_DEVICE 0x0D
#define GAP_SIMPLE_PAIRING_HASH_C 0x0E
#define GAP_SIMPLE_PAIRING_HASH_C192 0x0E
#define GAP_SIMPLE_PAIRING_RANDOMIZER_R 0x0F
#define GAP_SIMPLE_PAIRING_RANDOMIZER_R192 0x0F
#define GAP_DEVICE_ID 0x10
#define GAP_SECURITY_MANAGER_TK_VALUE 0x10
#define GAP_SECURITY_MANAGER_OUT_OF_BAND_FLAGS 0x11
#define GAP_SLAVE_CONNECTION_INTERVAL_RANGE 0x12
#define GAP_SERVICE_SOLICITATION_16BITS_UUID_LIST 0x14
#define GAP_SERVICE_SOLICITATION_128BITS_UUID_LIST 0x15
#define GAP_SERVICE_DATA_16BITS_UUID 0x16
#define GAP_PUBLIC_TARGET_ADDRESS 0x17
#define GAP_RANDOM_TARGET_ADDRESS 0x18
#define GAP_APPEARANCE 0x19
#define GAP_ADVERTISING_INTERVAL 0x1A
#define GAP_LE_BLUETOOTH_DEVICE_ADDRESS 0x1B
#define GAP_LE_ROLE 0x1C
#define GAP_SIMPLE_PAIRING_HASH_C256 0x1D
#define GAP_SIMPLE_PAIRING_RANDOMIZER_R256 0x1E
#define GAP_SERVICE_SOLICITATION_32BITS_UUID_LIST 0x1F
#define GAP_SERVICE_DATA_32BITS_UUID 0x20
#define GAP_SERVICE_DATA_128BITS_UUID 0x21
#define GAP_LE_SECURE_CONNECTION_CONFIRMATION_VALUE 0x22
#define GAP_LE_SECURE_CONNECTION_RANDOM_VALUE 0x23
#define GAP_URI 0x24
#define GAP_INDOOR_POSITIONING 0x25
#define GAP_TRANSPORT_DISCOVERY_DATA 0x26
#define GAP_LE_SUPPORTED_FEATURES 0x27
#define GAP_CHANNEL_MAP_UPDATE_INDICATION 0x28
#define GAP_PB_ADV 0x29
#define GAP_MESH_MESSAGE 0x2A
#define GAP_MESH_BEACON 0x2B
#define GAP_3D_INFORMATION_DATA 0x3D
#define GAP_MANUFACTURER_SPECIFIC_DATA 0xFF

// Helper functions to fill up the advertisement packets
#include <oble/adv.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

bool obleGapAppendFlag(obleAdv_t *adv, obleAdvPacketType_t packetType, uint8_t flags);

bool obleGapAppendLocalName(obleAdv_t *adv, obleAdvPacketType_t packetType, char* name);

bool obleGapAppendManufacturerData(obleAdv_t *adv, obleAdvPacketType_t packetType, uint16_t manufacturerId, char* data, int length);

#endif /* end of include guard: __OBLE_GAP_H__ */
