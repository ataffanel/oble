// ATT table interface: This file is an interface and can have multiple
// implementation. It contains the functions to be implemented for the ATT
// server to work.

#ifndef __ATT_TABLE_H__
#define __ATT_TABLE_H__

#include <oble/uuid.h>
#include <stddef.h>

// ATT table element. The read and write functions returns an error:
// 0 if success, >0 if an error occured. The error code is defined by the
// bluetooth specification and is sent back to the ATT server
// The 'context' pointer could be anything useful to the att table
// implementation, the att server passes it to the read/write function.
// For example for a GATT server, for a service definition the read funtion
// could be "serviceDefinitionRead" and the context set to the address of the
// service structure.
struct attElement_s {
  uint16_t handle;
  uuid_t uuid;
  void* context;
  int (*read)(void* context, char* buffer, int* length, int maxLength);
  int (*write)(void *context, char* buffer, int length);

  // Returns the current length of the attribute
  int (*getLength)(void *context);

  // Support for linked list (optionally implemented by higher layer)
  struct attElement_s *next;
};

struct attElement_s * obleAttTableGetNextElement(int start, int end);
struct attElement_s * obleAttTableGetElementByType(int start, int end, uuid_t uuid);
struct attElement_s * obleAttTableGetElementByTypeValue(int start, int end, uuid_t uuid,
                                                        size_t length, char* value);
int obleAttTableGetGroupByType(int start, int end, uuid_t uuid, uint16_t *handle, uint16_t *endGroup);

#define obleAttTableGetElement(handle) obleAttTableGetNextElement(handle, handle)
#endif //__ATT_TABLE_H__
