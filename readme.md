OBLE: Experimental Bluetooth LE stack for nRF51
===============================================

Experimental implementation of a BLE stack for the nRF51822.

This project aims to implement a C BLE stack that is OS independent
(ie. mainly standalone and easily portable to other project/mcu).
The goal is to be able to use this stack for the Crazyflie 2.0 quadcopter.

Developed on a BBC Micro:bit board.
This repos is currently mainly an example program.
The OBLE lib will eventually be broken down to work as a standalone lib.
The example currently advertise (fake) temperature and battery level.

The stack handles advertisement, connection and ATT/GATT read.

## Supported functionalities

The stack implements Bluetooth Low Energy 4.2. The lists bellow lists mostly
optional functionalities, mandatory functionalities are implemented (for example
supporting connection in slave mode requires a couple of link layer procedure
to be implemented).

### Link layer

|Functionality                     | Supported | Note |
|----------------------------------|:---------:|------|
| Advertising                      | ✓         |      |
| Master/host                      |           |      |
| Slave/device                     | ✓         |      |
| Encryption                       |           |      |
| Connection parameter request     |           |      |
| Slave initiated feature exchange |           |      |
| LE Ping                          |           |      |
| LE data packet length extention  |           |      |
| LL Privacy                       |           |      |

### ATT Server

| Request/Command                  | Supported | Note |
|----------------------------------|:---------:|------|
| MTU Exchange                     | ✓         |      |
| Find information                 | ✓         |      |
| Find by type value               | ✓         |      |
| Read by type                     | ✓         |      |
| Read                             | ✓         |      |
| Read blob                        |           |      |
| Read multiple                    |           |      |
| Read by group type               | ✓         |      |
| Write                            | ✓         |      |
| Write command                    | ✓         |      |
| Signed write command             |           |      |
| Queue write                      |           |      |
| Value notification               |           |      |
| Value indication                 |           |      |

### GATT Server

| Sub-Procedures                            | Supported | Note      |
|-------------------------------------------|:---------:|-----------|
| Exchange MTU                              | ✓         |           |
| Discover all primary services             | ✓         | Mandatory |
| Discover primary services by UUID         | ✓         | Mandatory |
| Find included services                    | ✓         | Mandatory |
| Discover all characteristic of a service  | ✓         | Mandatory |
| Discover characteristic by UUID           | ✓         | Mandatory |
| Discover all characteristic descriptors   | ✓         | Mandatory |
| Read characteristic value                 | ✓         | Mandatory |
| Read using characteristic UUID            | ✓         | Mandatory |
| Read long characteristic value            |           |           |
| Read multiple characteristic value        |           |           |
| Write without response                    | ✓         |           |
| Signed write without response             |           |           |
| Write characteristic value                | ✓         |           |
| Write long characteristic value           |           |           |
| Characteristic value reliable writes      |           |           |
| Notifications                             |           |           |
| Indications                               |           |           |
| Read characteristic descriptors           | ✓         |           |
| Read long characteristic descriptors      |           |           |
| Write characteristics descriptors         |           |           |
| Write long characteristic descriptors     |           |           |
